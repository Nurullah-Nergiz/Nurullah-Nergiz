# 💫 About Me:
Hello, I'm a front-end developer from Turkey named Nurullah. Since 2019, I have worked with web technologies.<br>Visit my portfolio for additional details. 

[![Website](https://img.shields.io/badge/website-000000?logo=Website&logoColor=white)](https://nurullahnergiz.com/)
[![LinkedIn](https://img.shields.io/badge/LinkedIn-%230077B5.svg?logo=linkedin&logoColor=white)](https://linkedin.com/in/nurullah-nergiz) 

## 🚀 Skills
* HTML5, CSS3, JavaScript
* ReactJS
* VueJS
* TailwindCSS
* NodeJS
* ExpressJS
* MongoDB
* MySQL
* Git
* GitHub, GitLab
* Postman
* Docker (basic)

---
<details>
  <summary>More...</summary>
<br>
<img src="https://github-readme-stats.vercel.app/api/top-langs/?username=Nurullah-nergiz&theme=dark&hide_border=true&include_all_commits=true&count_private=true&layout=compact">
<br>
<img src="https://github-readme-streak-stats.herokuapp.com/?user=Nurullah-Nergiz&theme=dark&hide_border=true">

[![](https://visitcount.itsvg.in/api?id=Nurullah-Nergiz&icon=0&color=0)](https://visitcount.itsvg.in)
</details>
